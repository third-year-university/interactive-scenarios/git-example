<?php
    namespace app;
    session_start();
    if (!isset($_SESSION['authorised'])){
        $_SESSION['authorised'] = false;
        $_SESSION['user_id'] = -1;
    }
    if (!isset($_SESSION['login_time'])){
        $_SESSION['login_time'] = false;
    }

    if (!$_SESSION['authorised'] or !$_SESSION['login_time']){
        header('Location: login.php', true, 303);
    }
    if (time() - $_SESSION['login_time'] > 1200){
        $_SESSION['authorised'] = false;
        $_SESSION['login_time'] = false;
        $_SESSION['user_id'] = -1;
        session_regenerate_id(true);
        header('Location: login.php', true, 303);
    }
    $_SESSION['login_time'] = time();

    if ($_GET){
        if (key_exists('logoff', $_GET)){
            $_SESSION['authorised'] = false;
            $_SESSION['login_time'] = false;
            $_SESSION['user_id'] = -1;
            session_regenerate_id(true);
            header('Location: login.php', true, 303);
        }
    }
?>

<!DOCTYPE html>
<html lang="ru">
    <head>
        <meta charset="UTF-8">
        <link rel="stylesheet" type="text/css" href="css/main.css">
        <link rel="stylesheet" type="text/css" href="css/task.css">
        <title>Календарь</title>
        <script src="scripts/main.js"></script>
    </head>
    <body>
        <div class="main_box">
            <div class="upper_line">
                <h1>Мой календарь</h1>
                <a class="logoff_icon" href="?logoff"><img class="logoff_icon" src="images/icons/logoff.svg"></a>
            </div>
            <div class="create_line">
                <h2>Список задач</h2>
                <img id="add_icon" class="add_icon" src="images/icons/add.svg">
            </div>
            <form>
                <div class="header_line">
                    <select id="task_choice" class="task_choice">
                        <option value="1">Текущие задачи</option>
                        <option value="2">Просроченные задачи</option>
                        <option value="3">Выполненные задачи</option>
                        <option value="4">Выбрать дату</option>
                    </select>
                    <input disabled id="date_choice" type="date">
                    <div class="period_options">
                        <div id="today" class="period_option clickable">
                            сегодня
                        </div>
                        <div id="tomorrow" class="period_option clickable">
                            завтра
                        </div>
                        <div id="in_week" class="period_option clickable">
                            через неделю
                        </div>
                        <div id="in_2_week" class="period_option clickable">
                            через две недели
                        </div>
                    </div>
                </div>
            </form>
            <div class="tasks_table">
                <div class="header row">
                    <div class="cell">
                        Тип
                    </div>
                    <div class="cell">
                        Задача
                    </div>
                    <div class="cell">
                        Место
                    </div>
                    <div class="cell">
                        Дата и время
                    </div>
                </div>
                <div id="tasks_table" class="scroll_area">
                    <div class="row">
                        <div class="cell">
                            Дело
                        </div>
                        <div class="cell">
                            разработать приложение
                        </div>
                        <div class="cell">
                            ИГУ
                        </div>
                        <div class="cell">
                            25.05.2022
                        </div>
                    </div>
                    <div class="row">
                        <div class="cell">
                            Дело
                        </div>
                        <div class="cell">
                            разработать приложение
                        </div>
                        <div class="cell">
                            ИГУ
                        </div>
                        <div class="cell">
                            25.05.2022
                        </div>
                    </div>

                </div>
            </div>
        </div>
        <div id="cover" class="cover">
        </div>
        <div id="task_box" class="task_box">
            <from name="form" id="form">
                <div class="upper_line">
                    <h2>Задача</h2>
                    <img id="cross" class="cross_icon" src="images/icons/cross.svg">
                </div>
                <div class="input_line">
                    <img alt="user_icon" class="form_icon" src="images/icons/topic.svg">
                    <input id="topic" name="topic" placeholder="Тема" type="text" value="" required>
                </div>
                <div class="input_line">
                    <img alt="type_icon" class="form_icon" src="images/icons/type.svg">
                    <select id="type" name="type" required>
                        <option value="" selected disabled>Тип</option>
                        <option value="1">Встреча</option>
                        <option value="2">Звонок</option>
                        <option value="3">Совещание</option>
                        <option value="4">Дело</option>
                    </select>
                </div>
                <div class="input_line">
                    <img alt="location_icon" class="form_icon" src="images/icons/location.svg">
                    <input id="location" name="location" placeholder="Место" type="text" value="" required>
                </div>
                <div id="date_time_line" class="input_line">
                    <img alt="datetime_icon" class="form_icon" src="images/icons/datetime.svg">
                    <input id="date" type="date" required>
                    <input id="time" type="time" required>

                </div>
                <div class="input_line">
                    <img alt="sandclocks_icon" class="form_icon" src="images/icons/sand_clocks.svg">
                    <select id="duration" name="duration" required>
                        <option value="" selected disabled>Длительность</option>
                        <option value="1">10 минут</option>
                        <option value="2">30 минут</option>
                        <option value="3">1 час</option>
                        <option value="4">2 часа</option>
                        <option value="5">Весь день</option>
                    </select>
                </div>
                <div class="input_line">
                    <img alt="user_icon" class="form_icon" src="images/icons/checklist.svg">
                    <select id="status" name="status" required>
                        <option value="" selected disabled>Статус</option>
                        <option value="0">В процессе</option>
                        <option value="1">Выполнено</option>
                    </select>
                </div>
                <div id="comment" class="input_line">
                    <img alt="user_icon" class="form_icon" src="images/icons/comment.svg">
                    <textarea id="comment2" placeholder="Комментарий"></textarea>
                </div>
                <div class="input_line">
                    <input id="submit" type="submit" value="Сохранить" name="submit">
                </div>
            </from>
        </div>
    </body>
</html>
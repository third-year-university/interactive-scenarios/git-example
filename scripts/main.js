let add_icon, cover, task_box, cross;
let submit, date, time, topic, t_ype, l_ocation, duration, status, comment;
let task_id;
let task_choice, date_choice, today, tomorrow, in_week, in_2_week, tasks_table;

let selected = 1;

function showAddNewTask(bla, user_id= 0) {
    task_id = user_id;
    cover.classList.add('show');
    task_box.classList.add('show');
    if (task_id === 0){
        let date_now = new Date();
        date_now.setDate(date_now.getDate() + 1);
        date.value = date_now.toISOString().substring(0, 10);
        time.value = '10:10';
    }
    else{
        let xhr = new XMLHttpRequest();
        let formData = new FormData();
        formData.append('method', 'get_tasks_for_id');
        formData.append('task_id', task_id);
        xhr.open("POST", "API.php");
        xhr.send(formData);
        xhr.onload = function (){
            let result = JSON.parse(xhr.response)[0];
            topic.value = result.topic;
            t_ype.value = result.type;
            l_ocation.value = result.location;
            date.value = result.datetime.substring(0, 10);
            time.value = result.datetime.substring(11, 16);
            duration.value = result.duration;
            status.value = result.status;
            comment.value = result.comment;
        };
    }
}

function hideAddNewTask() {
    cover.classList.remove('show');
    task_box.classList.remove('show');
}

function saveTask() {
    let values = new Map();
    values.set('datetime', parseInt(Date.parse(values.get('date') + " " + values.get('time'))/1000, 10));

    let formData = new FormData();
    if (date.value === '' || time.value === '' || topic.value === '' || t_ype.value === '' || l_ocation.value === '' || duration.value === '' || status.value === ''){
        return;
    }

    formData.append('topic', topic.value);
    formData.append('type', t_ype.value);
    formData.append('location', l_ocation.value);
    formData.append('duration', duration.value);
    formData.append('status', status.value);
    formData.append('comment', comment.value);

    formData.append('datetime', parseInt(Date.parse(date.value + " " + time.value)/1000, 10));
    formData.append('id', task_id);
    formData.append('method', 'save_task');

    //console.log(t_ype);

    let xhr = new XMLHttpRequest();
    xhr.open("POST", "API.php");
    xhr.send(formData);
    //xhr.onload = () => alert(xhr.response);
    hideAddNewTask();
    check_selected();
}

function add_zeroes(num){
    if (num.length === 1){
        return '0' + num;
    }
    return num;
}

function format_date(date){
    let my_date = new Date(date);
    let days = add_zeroes(my_date.getDate().toString());
    let months = add_zeroes((my_date.getMonth() + 1).toString());
    let years = add_zeroes(my_date.getFullYear().toString());
    let hours = add_zeroes(my_date.getHours().toString());
    let minutes = add_zeroes(my_date.getMinutes().toString());

    return `${days}.${months}.${years} ${hours}:${minutes}`;
}

function apply_result(response){
    let results = JSON.parse(response);
    tasks_table.innerHTML = '';
    for (let i = 0; i < results.length; i++){
        let str_type = '';
        let num_type = parseInt(results[i].type, 10);
        if (num_type > 4 || num_type < 1){
            str_type = 'N/A';
        }
        else{
            let types = new Map([['1', 'Встреча'], ['2', 'Звонок'], ['3', 'Совещание'], ['4', 'Дело']]);
            str_type = types.get(results[i].type);
        }
        let str_topic = results[i].topic;
        let str_location = results[i].location;
        let str_datetime = format_date(results[i].datetime);
        let elem = document.createElement('div');
        elem.classList.add('row');
        elem.setAttribute('task_id', results[i].id);
        elem.innerHTML = `
                        <div class="cell">
                            ${str_type}
                        </div>
                        <div class="cell">
                            ${str_topic}
                        </div>
                        <div class="cell">
                            ${str_location}
                        </div>
                        <div class="cell">
                            ${str_datetime}
                        </div>`;
        elem.addEventListener('click', task_click);
        tasks_table.appendChild(elem);
    }
}

function task_click(e){
    showAddNewTask('asdasd', e.target.parentElement.getAttribute('task_id'));
}

function update_tasks_to_do(){
    get_tasks('get_tasks_to_do');
}

function update_tasks_succeed() {
    get_tasks('get_tasks_succeed');
}

function update_tasks_deadlined(){
    get_tasks('get_tasks_deadlined');
}

function check_selected(){
    if (selected === '1'){
        update_tasks_to_do();
    }
    if (selected === '2'){
        update_tasks_deadlined();
    }
    if (selected === '3'){
        update_tasks_succeed();
    }
    if (selected === '4'){
        update_specific_date();
        date_choice.disabled = false;
    }
    else{
        date_choice.disabled = true;
    }

    all_clickable();

}

function all_clickable(){
    today.classList.add('clickable');
    tomorrow.classList.add('clickable');
    in_week.classList.add('clickable');
    in_2_week.classList.add('clickable');
}

function today_click() {
    let date_now = new Date();
    date_choice.value = date_now.toISOString().substring(0, 10);
    all_clickable();
    task_choice.value = '4';
    selected = '4';
    check_selected();
    today.classList.remove('clickable');
}

function tomorrow_click() {
    let date_now = new Date();
    date_now.setDate(date_now.getDate() + 1);
    date_choice.value = date_now.toISOString().substring(0, 10);
    all_clickable();
    task_choice.value = '4';
    selected = '4';
    check_selected();
    tomorrow.classList.remove('clickable');
}

function in_week_click() {
    let date_now = new Date();
    date_now.setDate(date_now.getDate()  + 7);
    date_choice.value = date_now.toISOString().substring(0, 10);
    all_clickable();
    task_choice.value = '4';
    selected = '4';
    check_selected();
    in_week.classList.remove('clickable');
}

function in_2_week_click() {
    let date_now = new Date();
    date_now.setDate(date_now.getDate()  + 14);
    date_choice.value = date_now.toISOString().substring(0, 10);
    all_clickable();
    task_choice.value = '4';
    selected = '4';
    check_selected();
    in_2_week.classList.remove('clickable');
}

function get_tasks($method){
    let xhr = new XMLHttpRequest();
    let formData = new FormData();
    formData.append('method', $method);
    xhr.open("POST", "API.php");
    xhr.send(formData);
    xhr.onload = () => apply_result(xhr.response);
}

function update_specific_date(){
    all_clickable();
    let xhr = new XMLHttpRequest();
    let formData = new FormData();
    formData.append('method', 'get_tasks_for_specific_date');
    formData.append('datetime', date_choice.value);
    xhr.open("POST", "API.php");
    xhr.send(formData);
    xhr.onload = () => apply_result(xhr.response);
}

function tasks_changing(){
    selected = task_choice.value;
    check_selected();
}

function init_vars(){
    add_icon = document.getElementById('add_icon');
    cover = document.getElementById('cover');
    task_box = document.getElementById('task_box');
    cross = document.getElementById('cross');

    date = document.getElementById('date');
    time = document.getElementById('time');
    submit = document.getElementById('submit');
    topic = document.getElementById('topic');
    t_ype = document.getElementById('type');
    l_ocation = document.getElementById('location');
    duration = document.getElementById('duration');
    status = document.getElementById('status');
    comment = document.getElementById('comment2');

    task_choice = document.getElementById('task_choice');
    date_choice = document.getElementById('date_choice');
    today = document.getElementById('today');
    tomorrow = document.getElementById('tomorrow');
    in_week = document.getElementById('in_week');
    in_2_week = document.getElementById('in_2_week');
    tasks_table = document.getElementById('tasks_table');
}


function init_events(){
    add_icon.addEventListener('click', showAddNewTask);
    cross.addEventListener('click', hideAddNewTask);
    submit.addEventListener('click', saveTask);
    task_choice.addEventListener('change', tasks_changing);
    date_choice.addEventListener('change', update_specific_date);
    today.addEventListener('click', today_click);
    tomorrow.addEventListener('click', tomorrow_click);
    in_week.addEventListener('click', in_week_click);
    in_2_week.addEventListener('click', in_2_week_click);

}

function init(){
    init_vars();
    init_events();
    update_tasks_to_do();
    let date_now = new Date();
    date_choice.value = date_now.toISOString().substring(0, 10);
}

document.addEventListener("DOMContentLoaded", init);
<?php

    namespace app;

    include("DbConnector.php");

    if (!isset($_SESSION['authorised'])) {
        $_SESSION['authorised'] = false;
        $_SESSION['user_id'] = -1;
    }
    if (!isset($_SESSION['login_time'])) {
        $_SESSION['login_time'] = false;
    }
    if ($_SESSION['authorised']) {
        header('Location: index.php', true, 303);
    }

    $ini = parse_ini_file('config.ini');
    $db_host = $ini['db_host'];
    $db_name = $ini['db_name'];
    $db_login = $ini['db_login'];
    $db_password = $ini['db_password'];
?>


<!DOCTYPE html>

<html lang="ru">
    <head>
        <meta charset="UTF-8">
        <title>Регистрация в календаре</title>
        <link rel="stylesheet" type="text/css" href="./css/main.css">
        <link rel="stylesheet" type="text/css" href="./css/login.css">
        <link rel="stylesheet" type="text/css" href="./css/task.css">
    </head>
    <body>
    <?php
        $is_ok = true;
        $login = '';
        $password = '';
        $email = '';
        if ($_POST){
            if (key_exists('login', $_POST)){
                $login = htmlspecialchars($_POST['login']);
            }
            if (key_exists('password', $_POST)){
                $password = htmlspecialchars($_POST['password']);
            }
            if (key_exists('email', $_POST)){
                $email = htmlspecialchars($_POST['email']);
            }

            $db_connector = new DbConnector($db_host, $db_name, $db_login, $db_password);
            if ($db_connector->register_user($login, md5($password), $email)){
                header('Location: login.php', true, 303);
            }
            else{
                $is_ok = false;
            }
        }
    ?>
    <div id="registration" class="main_box">
        <h1>Регистрация</h1>
        <form action="register.php" method="post">
            <div class="input_line">
                <img alt="user_icon" class="form_icon" src="images/icons/user.svg">
                <input name="login" placeholder="Логин" type="text" maxlength="50" value="<?php echo $login ?>" required>
            </div>
            <div class="input_line">
                <img alt="password_icon" class="form_icon" src="images/icons/password.svg">
                <input name="password" placeholder="Пароль" type="password" maxlength="50" value="" required>
            </div>
            <div class="input_line">
                <img alt="mail_icon" class="form_icon" src="images/icons/mail.svg">
                <input name="email" placeholder="Email" type="email" maxlength="50" value="<?php echo $email ?>" required>
            </div>

            <div class="input_line">
                <input id="submit" type="submit" value="Зарегистрироваться" name="submit">
            </div>
            <div id="incorrect_login" class="input_line">
                Логин и/или email уже используется
            </div>

        </form>

        <?php
        if (!$is_ok){
            echo "<style>#incorrect_login{
                                                    display: block !important;
                                              }</style>";
        }

        ?>
        <a class="registration" href="login.php">Вход</a>
    </div>
    </body>
</html>



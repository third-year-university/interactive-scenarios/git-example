<?php

namespace app;

use PDO;

include ("Task.php");

class DbConnector
{
    protected string $host;
    protected string $database;
    protected string $login;
    protected string $password;
    protected $db;
    public function __construct($host, $database, $login, $password){
        $this->host = $host;
        $this->database = $database;
        $this->login = $login;
        $this->password = $password;
        $this->db = new PDO(sprintf('mysql:dbname=%s;host=%s', $this->database, $this->host),
            $this->login, $this->password);
    }

    public function sql_response_to_tasks($stmt): array
    {
        $results = $stmt->fetchAll(PDO::FETCH_ASSOC);
        $tasks = [];
        foreach ($results as $key => $result) {
            $tasks[] = new Task($result['id'], $result['topic'], $result['type'], $result['location'], $result['datetime'],
                $result['duration'], $result['comment'], $result['status'], $result['user_id']);
        }
        return $tasks;
    }

    public function init_tables(): void
    {
        if (!$this->is_table_exists('users')){
            $this->create_users_table();
        }
        if (!$this->is_table_exists('tasks')){
            $this->create_tasks_table();
        }
    }


    protected function is_table_exists($table_name): bool{
        $return_value = true;
        $stmt = $this->db->prepare("CHECK TABLE `?` QUICK;");
        $stmt->execute([$table_name]);
        $results = $stmt->fetchAll(PDO::FETCH_ASSOC);
        $text = $results[0]['Msg_text'];
        if ($text == sprintf("Table '%s.'%s'' doesn't exist", $this->database, $table_name)){
            $return_value = false;
        }
        return $return_value;
    }


    protected function create_users_table(): void
    {
        $stmt = $this->db->prepare("
            CREATE TABLE `users`(
                `id` INT(10) UNSIGNED NOT NULL PRIMARY KEY AUTO_INCREMENT,
                `username` VARCHAR(255) NOT NULL,
                `email` VARCHAR(255) NOT NULL,
                `password` VARCHAR(255) NOT NULL,
                `last_login` DATETIME DEFAULT NULL
            );
        ");
        $stmt->execute();
        return;
    }

    protected function create_tasks_table(): void
    {
        $stmt = $this->db->prepare("
            CREATE TABLE `tasks`(
                `id` INT(10) UNSIGNED NOT NULL PRIMARY KEY AUTO_INCREMENT,
                `topic` VARCHAR(255) NOT NULL,
                `type` TINYINT NOT NULL,
                `location` VARCHAR(255) NOT NULL,
                `datetime` DATETIME NOT NULL,
                `duration` TINYINT NOT NULL,
                `comment` MEDIUMTEXT NOT NULL,
                `status` TINYINT NOT NULL,
                `user_id` INT UNSIGNED NOT NULL,
                FOREIGN KEY (`user_id`) REFERENCES `users` (`id`)
            );
        ");
        $stmt->execute();
        return;
    }

    public function register_user($username, $password, $email): bool
    {
        $stmt =$this->db->prepare("
            SELECT COUNT(*) FROM `users` WHERE `username` = ? OR `email` = ? LIMIT 1;
        ");
        $stmt->execute([$username, $email]);
        $result = $stmt->fetch(PDO::FETCH_ASSOC)['COUNT(*)'];
        if (intval($result) > 0){
            return false;
        }
        $stmt = $this->db->prepare("
            INSERT INTO `users`(`username`, `email`, `password`) VALUES(?, ?, ?);
        ");
        $stmt->execute([$username, $email, $password]);
        return true;
    }

    public function authorise_user($username, $password): bool
    {
        $stmt = $this->db->prepare("
            SELECT COUNT(*) FROM `users` WHERE `username` = ? AND `password` = ? LIMIT 1;
        ");
        $stmt->execute([$username, $password]);
        $result = $stmt->fetch(PDO::FETCH_ASSOC)['COUNT(*)'];
        if (intval($result) == 0){
            return false;
        }
        $stmt = $this->db->prepare("
            UPDATE `users` SET `last_login` = NOW() WHERE `username` = ?;
        ");
        $stmt->execute([$username]);
        return true;
    }

    public function get_tasks_to_do($user_id): array
    {
        $stmt = $this->db->prepare("
            SELECT `id`, `topic`, `type`, `location`, `datetime`, `duration`, `comment`, `status`, `user_id`
            FROM `tasks` WHERE `datetime` > NOW() AND `status` = 0 AND `user_id` = ?;
        ");
        $stmt->execute([$user_id]);
        //var_dump($stmt->fetchAll(PDO::FETCH_ASSOC));
        return $this->sql_response_to_tasks($stmt);
    }

    public function get_tasks_succeed($user_id): array
    {
        $stmt = $this->db->prepare("
            SELECT `id`, `topic`, `type`, `location`, `datetime`, `duration`, `comment`, `status`, `user_id`
            FROM `tasks` WHERE `status` = 1 AND `user_id` = ?;
        ");
        $stmt->execute([$user_id]);
        return $this->sql_response_to_tasks($stmt);
    }

    public function get_tasks_deadlined($user_id): array
    {
        $stmt = $this->db->prepare("
            SELECT `id`, `topic`, `type`, `location`, `datetime`, `duration`, `comment`, `status`, `user_id`
            FROM `tasks` WHERE `datetime` < NOW() AND `status` = 0 AND `user_id` = ?;
        ");
        $stmt->execute([$user_id]);
        return $this->sql_response_to_tasks($stmt);
    }

    public function get_tasks_for_specific_date($user_id, $date): array
    {
        $stmt = $this->db->prepare("
            SELECT `id`, `topic`, `type`, `location`, `datetime`, `duration`, `comment`, `status`, `user_id`
            FROM `tasks` WHERE DATE(`datetime`) = DATE(?) AND `user_id` = ?;
        ");
        $stmt->execute([$date, $user_id]);
        return $this->sql_response_to_tasks($stmt);
    }

    public function get_tasks_for_id($user_id, $task_id){
        $stmt = $this->db->prepare("
            SELECT `id`, `topic`, `type`, `location`, `datetime`, `duration`, `comment`, `status`, `user_id`
            FROM `tasks` WHERE `user_id` = ? AND `id` = ? LIMIT 1;
        ");
        $stmt->execute([$user_id, $task_id]);
        $result = $this->sql_response_to_tasks($stmt);
        if (sizeof($result) == 0){
            return false;
        }
        return $result;
    }

    public function add_task($task){
        $stmt = $this->db->prepare("
            INSERT INTO `tasks`(`topic`, `type`, `location`, `datetime`, `duration`, `comment`, `status`, `user_id`)
            VALUES (?, ?, ?, ?, ?, ?, ?, ?);
        ");
        $stmt->execute([$task->getTopic(), $task->getType(), $task->getLocation(), $task->getDatetime(),
            $task->getDuration(), $task->getComment(), $task->getStatus(), $task->getUserId()]);
        //var_dump($stmt->errorInfo());
        return;
    }

    public function edit_task($task){
        $stmt = $this->db->prepare("
            UPDATE `tasks` SET `topic` = ?, `type` = ?, `location` = ?, `datetime` = ?, `duration` = ?, `comment` = ?, `status` = ?
            WHERE `id` = ? AND `user_id` = ?;
        ");
        $stmt->execute([$task->getTopic(), $task->getType(), $task->getLocation(), $task->getDatetime(),
            $task->getDuration(), $task->getComment(), $task->getStatus(), $task->getId(), $task->getUserId()]);
        return;
    }

    public function get_id_by_username($username){
        $stmt = $this->db->prepare("
            SELECT `id` FROM `users` WHERE `username` = ? LIMIT 1;
        ");
        $stmt->execute([$username]);
        return $stmt->fetch(PDO::FETCH_ASSOC)['id'];
    }
}
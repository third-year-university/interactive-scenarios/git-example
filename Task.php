<?php

namespace app;

class Task
{
    protected $id;
    protected $topic;
    protected $type;
    protected $location;
    protected $datetime;
    protected $duration;
    protected $comment;
    protected $status;
    protected $user_id;

    public function __construct($id, $topic, $type, $location, $datetime, $duration, $comment, $status, $user_id)
    {
        $this->id = $id;
        $this->topic = $topic;
        $this->type = $type;
        $this->location = $location;
        $this->datetime = $datetime;
        $this->duration = $duration;
        $this->comment = $comment;
        $this->status = $status;
        $this->user_id = $user_id;
    }

    public function get_dict(): array
    {
        return array(
            "id" => $this->id,
            "topic" => $this->topic,
            "type" => $this->type,
            "location" => $this->location,
            "datetime" => $this->datetime,
            "duration" => $this->duration,
            "comment" => $this->comment,
            "status" => $this->status,
            "user_id" => $this->user_id
        );
    }

    public function getId()
    {
        return $this->id;
    }

    public function getTopic()
    {
        return $this->topic;
    }
    public function getType()
    {
        return $this->type;
    }
    public function getLocation()
    {
        return $this->location;
    }

    public function getDatetime()
    {
        return $this->datetime;
    }

    public function getDuration()
    {
        return $this->duration;
    }

    public function getComment()
    {
        return $this->comment;
    }

    public function getStatus()
    {
        return $this->status;
    }
    public function getUserId()
    {
        return $this->user_id;
    }


}